function displayPuzzles() {
  $.get('./puzzles/', function(data) {
    var names = []
    var re = /<a[^>]*>([^<]+)\.json<\/a>/ig
    var match
    while (match = re.exec(data))
      names.push(match[1])

    var write = '<p>Select a puzzle from the server:</p>'
    write += '<ul>'
    for (var i = 0; i < names.length; i++) {
      write += '<li class="puzzlink" id="' + names[i] + '.json">' + names[i] + '</li>'
    }
    write += '</ul>'
    $('#puzzles').prepend(write)
  })
}

function displayColors() {
  /* Displaying the colors */
  var writing = '<table><tr>'
  for (var i = 0; i < colors.length; i++) {
    var color = colors[i]
    writing += '<td class="color" id="color-' + color + '" style="background-color:#' + color + '"></td>'
  }
  $('#colors').html(writing + '</tr></table>')
  updateColors();
}

function generateHints() {
  for (var i = 0; i < game.height; i++) {
    var writing = ''
    game.getRowHints(i,selectedColor).forEach(function (hint) {
      writing += '<span class="hint" id="hint' + hint.no + '">' + hint.number + '</span>'
    })
    $('#rowHint' + i).html(writing)
  }

  for (var j = 0; j < game.width; j++) {
    var writing = ''
    game.getColHints(j,selectedColor).forEach(function (hint) {
      writing += '<span class="hint" id="hint' + hint.no + '">' + hint.number + '</span>'
    })
    $('#colHint' + j).html(writing)
  }

  for (var i = 0; i < game.height; i++)
    game.getRowHints(i,selectedColor).forEach(updateHint)
  for (var j = 0; j < game.width; j++)
    game.getColHints(j,selectedColor).forEach(updateHint)
}

function displayGrid() {
  /* Writing the column hints */
  var writing = ''
  writing += '<table>'
  writing += '<tr>'
  writing += '<td id="gridcorner"></td>'
  /* Col hints */
  for (var j = 0; j < game.width; j++)
    writing += '<td class="colHints" id="colHint' + j + '" data-y="' + j + '"></td>'
  writing += '</tr>'

  /* Writing the grid */
  for (var i = 0; i < game.height; i++) {
    writing += '<tr>'
    /* Row hints */
    writing += '<td class="lineHints" id="rowHint' + i + '" data-x="' + i + '"></td>'

    for (var j = 0; j < game.width; j++) {
      writing += '<td class="tile" id="tile-' + i + '-' + j + '"'
      writing += ' data-x="' + i + '" data-y="' + j + '""></td>'
    }
    writing += '</tr>'
  }
  writing += '</table>'
  $('#grid').html(writing)

  generateHints()
  updateTiles()
  /* Allocate space in the grid top left corner of the canvas */
  resizeGridCorner()
}

// Display all except puzzles
function displayAll() {
  displayColors()
  displayCanvas()
  displayGrid()
}


function updateColor(c) {
  var element = $('#color-' + c);
  element.removeClass('validColor invalidColor selectedColor');
  if (game.colors[c].valid == 'valid')
    element.addClass('validColor')
  else if (game.colors[c].valid == 'invalid')
    element.addClass('invalidColor')
}

function updateColors() {
  for (var c in game.colors)
    updateColor(c)
  $('#color-' + selectedColor).addClass('selectedColor')
}

function updateHint(hint) {
  var element = $('#hint' + hint.no)
  element.removeClass('validHint invalidHint')
  if (hint.state == 'valid')
    element.addClass('validHint')
  else if (hint.state == 'invalid')
    element.addClass('invalidHint')
}

function updateHintsAt(x, y) {
  game.getRowHints(x,selectedColor).forEach(updateHint)
  game.getColHints(y,selectedColor).forEach(updateHint)
}

function updateTileAt(x, y) {
  var element = $('#tile-' + x + '-' + y)
  /* Reset tile */
  element.css('background-color', '')
  element.css('border-color', '')
  element.removeClass('crossed highlight')
  /* Set contents */
  if (game.isFilled(y,x)) {
    color = game.get(y,x)
    element.css('background-color', '#' + color)
    element.css('border-color', '#' + color)
    if (color == selectedColor)
      element.addClass('highlight')
  }
  else if (game.isCrossed(y,x,selectedColor)) {
    element.css('color', '#' + selectedColor)
    element.addClass('crossed')
  }
}

function updateTiles() {
  /* Updating tiles */
  for (var i = 0; i < game.height; i++) {
    for (var j = 0; j < game.width; j++) {
      updateTileAt(i, j);
    }
  }
}

function updateCursor(cell) {
  var $grid = $('#grid')
  var table = cell.closest('table')
  // Extract cursor coordinates
  var x = cell.attr('data-x'), y = cell.attr('data-y')
  // Remove cursor and all highlighted blocks
  $('#rowcursor, #colcursor, .rowblock, .colblock').remove()

  if (x) {
    $('<div id="rowcursor"></div>').css({
      position: 'absolute',
      left: table.position().left,
      top: cell.position().top - 2,
      width: table.width(),
      height: cell.height()
    }).appendTo($grid)

    var blocks = game.getRowBlocks(x,selectedColor).filter(isFilledBlock)
    for (var i = 0; i < blocks.length; i++) {
      var block = blocks[i]
      var start = $('#tile-' + x + '-' + block.start)
      var end = $('#tile-' + x + '-' + (block.start + block.size - 1))
      $('<div class="rowblock"><span>' + block.size + '</span></div>').css({
        position: 'absolute',
        left: start.position().left -1,
        top: cell.position().top - 1,
        width: end.position().left + end.width() - start.position().left,
        height: cell.height()
      }).appendTo($grid)
    }
  }

  if (y) {
    $('<div id="colcursor"></div>').css({
      position: 'absolute',
      left: cell.position().left - 2,
      top: table.position().top,
      width: cell.width(),
      height: table.height()
    }).appendTo($grid)

    var blocks = game.getColBlocks(y,selectedColor).filter(isFilledBlock)
    for (var i = 0; i < blocks.length; i++) {
      var block = blocks[i]
      var start = $('#tile-' + block.start + '-' + y)
      var end = $('#tile-' + (block.start + block.size - 1) + '-' + y)
      $('<div class="colblock"><span>' + block.size + '</span></div>').css({
        position: 'absolute',
        left: cell.position().left - 1,
        top: start.position().top - 1,
        width: cell.width(),
        height: end.position().top + end.height() - start.position().top
      }).appendTo($grid)
    }
  }
}

function updateStats() {
  var chrono = ''
  var t = game.getElapsedTime()
  var days = Math.floor(t / (24*60*60*1000))
  var hours = Math.floor(t / (60*60*1000)) % 24
  var minutes = Math.floor(t / (60*1000)) % 60
  var seconds = Math.floor(t / 1000) % 60
  chrono =
    (days > 0 ? days + 'd ' : '') +
    (hours > 0 ? hours + 'h ' : '') +
    (minutes > 0 ? minutes + 'm ' : '') +
    seconds + 's'

  $('#chrono').text(chrono)

  $('#puzzle-title').text(game ? game.data.title : '-')
  $('#puzzle-difficulty').text(game ? game.data.difficulty: '-')
}

function updateCheatCount() {
  var displayed = game.cheatCount
  var suffix = ''
  if (game.cheatCount > 6) {
    displayed = 6
    suffix = '... (' + game.cheatCount + ')'
  }
  var text = Array(displayed+1).join('&#x2716;') + suffix
  $('#cheat-count').html(text)
}
