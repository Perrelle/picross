function loadPuzzle(file, onload) {
  var ext = file.name.split('.').pop()
  if (ext == 'bmp') {
    importBitmap(file, onload)
  }
  else {
    var reader = new FileReader()
    reader.addEventListener("load", function () {
        onload(JSON.parse(this.result))
      }, false)
    reader.readAsText(file)
  }
}

function importBitmap(file, onload) {
  var name = file.name.substring(0, file.name.indexOf('.'))
  loadBitmap(file, function (bitmap) {
    var puzzle = {
        title: name,
        author: 'unknown',
        credits: '',
        difficulty: 'unrated',
        grid: bitmap
      }
    onload(puzzle)
  })
}

function exportPuzzle(puzzle) {
  return '{\n' +
    '  "title":"' + puzzle.title + '",\n' +
    '  "author":"' + puzzle.author + '",\n' +
    '  "credits":"' + puzzle.credits + '",\n' + 
    '  "difficulty":"' + puzzle.difficulty + '",\n' +
    '  "grid":[\n' +
    puzzle.grid.map(function (line) {
      return '    ["' + line.join('","') + '"]'
    }).join(',\n') +
    '\n]}';
}

function download(filename, contents) {
  var element = $('<a/>', {
    href: 'data:text/plain;charset=utf-8,' + encodeURIComponent(contents),
    download: filename
  }).css({
    display: 'none'
  })
  element.appendTo($('body'))
  element.get()[0].click()
  element.remove()
}

function downloadPuzzle(puzzle) {
  var contents = exportPuzzle(puzzle)
  download(puzzle.title + '.json', contents)
}
