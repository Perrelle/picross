var pixelSize = 5

function displayCanvas() {
  var S = pixelSize
  var canvas = $('#canvas').get(0)
  canvas.width = game.width * S
  canvas.height = game.height * S

  for (var i = 0; i < game.height; i++)
    for (var j = 0; j < game.width; j++)
      updateCanvas(i,j)
}

function resizeGridCorner() {
  var S = pixelSize
  var corner = $('#gridcorner')
  corner.css("min-width", game.width * S + 4)
  corner.css("height", game.height * S + 4)
}

function updateCanvas(x, y) {
  var S = pixelSize
  var ctx = $('#canvas').get(0).getContext("2d")
  ctx.width = game.width * S
  ctx.height = game.height * S

  if (game.isFilled(y,x)) {
    var color = game.get(y,x)
    var rgb = colorToRgb(color)
    ctx.fillStyle = '#' + rgbToColor(rgbLerp(rgb, white, 0.8))
    ctx.fillRect(y * S, x * S, S, S)
    ctx.fillStyle = '#' + rgbToColor(rgbLerp(rgb, black, 0.8))
    ctx.fillRect(y * S + 1, x * S + 1, S - 1, S - 1)
    ctx.fillStyle = '#' + color
    ctx.fillRect(y * S + 1, x * S + 1, S - 2, S - 2)
  }
  else {
    ctx.clearRect(y * S, x * S, S, S)
  }
}

