// --- These functions are used to order colors ---

function rgb(r, g, b) {
  return {r: r, g: g, b: b}
}

var white = rgb(0xff, 0xff, 0xff)
var black = rgb(0x00, 0x00, 0x00)

function colorToRgb(color) {
  var result = /^([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
  if (result) {
    return {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
  }
  else {
    return null
  }
}

function rgbToColor(rgb) {
  var value =
    rgb.r * 0x010000 +
    rgb.g * 0x000100 +
    rgb.b * 0x000001 
  return ('000000' + value.toString(16)).slice(-6)
}

function valuateColor(color) {
  var rgb = colorToRgb(color)
  var M = Math.max(rgb.r, rgb.g, rgb.b)
  var m = Math.min(rgb.r, rgb.g, rgb.b)
  var C = M - m
  if (C == 0)
    return M / 0x80 - 2
  else if (M == rgb.r)
    return ((rgb.g - rgb.b) / C + 6) % 6
  else if (M == rgb.g)
    return ((rgb.b - rgb.r) / C + 2)
  else
    return ((rgb.r - rgb.g) / C + 4) 
}

function sortColor(color1, color2) {
  return valuateColor(color2) - valuateColor(color1);
}

function rgbLerp(rgb1, rgb2, c)
{
  return {
    r: Math.floor(rgb1.r * c + rgb2.r * (1 - c)),
    g: Math.floor(rgb1.g * c + rgb2.g * (1 - c)),
    b: Math.floor(rgb1.b * c + rgb2.b * (1 - c))
  }
}

function rgbDistance(rgb1, rgb2) {
  var dr = rgb1.r - rgb2.r
  var dg = rgb1.g - rgb2.g
  var db = rgb1.b - rgb2.b
  return Math.sqrt(dr * dr + dg * dg + db * db)
}
