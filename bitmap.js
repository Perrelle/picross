var similarityDistance = 16.0

function loadBitmap(file, onload) {
  output("Loading file", file.name, "...")
  var reader = new FileReader()
  reader.addEventListener("load", function () {
      try {
        var bitmap = readBitmap(this.result)
        reduceColors(bitmap)
        onload(bitmap)
      }
      catch (error) {
        output("Invalid bitmap or format not supported:", error)
      }      
    }, false)
  reader.readAsArrayBuffer(file)
}

function readBitmap(buffer) {
  // Extract header from file
  var data = new DataView(buffer)
  var header = {}
  header.magic_number  = data.getUint16(0x00, true)
  header.bitmap_start  = data.getUint32(0x0A, true)
  header.header_size   = data.getUint32(0x0E, true)
  header.bitmap_width  = data.getInt32(0x12, true)
  header.bitmap_height = data.getInt32(0x16, true)
  header.bpp           = data.getInt16(0x1C, true)
  header.compression   = data.getInt16(0x1E, true)

  // Interpret header
  if (header.magic_number != 0x4D42 || header.header_size < 40 ||
      header.compression != 0)
    throw "Unsupported format."

  if (header.bitmap_width > 100 || header.bitmap_height > 100 ||
      header.bitmap_width < 1   || header.bitmap_height < 1)
    throw "Image too large."

  var width = header.bitmap_width
  var height = header.bitmap_height
  var row_size = Math.ceil(3 * header.bitmap_width / 4) * 4

  // Exctract data
  var data = new Uint8Array(buffer, header.bitmap_start)

  // Convert data to bitmap
  var bitmap = new Array(height)
  for (var y = 0 ; y < height ; y++ ) {
    bitmap[y] = new Array(width)
    for (var x = 0 ; x < width ; x++) {
      var offset = x * 3 + row_size * (height - 1 - y)
      var c = rgb(data[offset+2], data[offset+1], data[offset])
      bitmap[y][x] = rgbToColor(c)
    }  
  }

  return bitmap
}

function countColors(bitmap) {
  var colors = {}
  for (var y = 0; y < bitmap.length ; y++) {
    for (var x = 0; x < bitmap[y].length; x++) {
      var c = bitmap[y][x];
      colors[c] = c in colors ? colors[c] + 1 : 1
    }
  }
  return colors
}

function mergeColors(bitmap, c1, c2) {
  output("Merging color", c2, "into dominant color", c1)
  for (var y = 0; y < bitmap.length ; y++) {
    for (var x = 0; x < bitmap[y].length; x++) {
      if (bitmap[y][x] == c2)
        bitmap[y][x] = c1
    }
  }
}

function reduceColors(bitmap) {
  var colors = countColors(bitmap)
  do {
    var changes = false
    for (var c1 in colors) {
      var nearest = c1
      var distance = similarityDistance
      for (var c2 in colors) {
        d = rgbDistance(colorToRgb(c1), colorToRgb(c2))
        if (c1 != c2 && d < distance) {
          nearest = c2
          distance = d
        }
      }
      if (nearest != c1) {
        c2 = nearest
        if (colors[c1] > colors[c2]) {
          mergeColors(bitmap, c1, c2)
          delete colors[c2]
        }
        else {
          mergeColors(bitmap, c2, c1)
          delete colors[c1]           
        }
        changes = true
        break;
      }
    }
  }
  while (changes)
}
