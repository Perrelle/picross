var puzzleHash = 0
var game = null
var selectedColor = 0
var colors = [] // colors value orderd by hue
var puzzlesBoxOpened = false
var pauseBox = null

var output = console.log.bind(window.console) // Used by bitmap.js


// --- Boxes ---

function show(element) {
  element.css('display', '')
  element.css('opacity', '1.0')
}

function hide(element, remove) {
  element.css('opacity', 0.0)
  if (remove)
    setTimeout(function () { element.remove() }, 500)
  else
    setTimeout(function () { element.css('display', 'none') }, 500)
}

function spawnBox(text, timeout, ephemeral) {
  var box = $('<div class="box"></div>').css('opacity', 0.0).text(text)
  box.appendTo('body')
  show(box)
  if (timeout)
    setTimeout(hide, timeout*1000, box, ephemeral)
  return box
}

function togglePuzzlesBox(display) {
  var box = $('#puzzles')
  puzzlesBoxOpened = display !== undefined ? display : !puzzlesBoxOpened
  if (puzzlesBoxOpened)
    show(box)
  else
    hide(box)
}


// --- Storage ---

function hash(s) {
  var h = 0
  for (var i = 0; i < s.length; i++)
    h = (h<<5)-h+s.charCodeAt(i)
  return h
}

function saveGame() {
  try {
    localStorage.setItem('puzzle' + puzzleHash, JSON.stringify(game.save()))
  }
  catch (e) {
    console.error(e)
  }
}

function quitGame() {
  game.pause()
  saveGame()
}

function loadGame(data) {
  if (game)
    quitGame()

  var gameState = null

  puzzleHash = hash(JSON.stringify(data.grid))

  try {
    var item = localStorage.getItem('puzzle' + puzzleHash)
    if (item)
      gameState = JSON.parse(item)
  }
  catch (e) {
    console.error(e)
  }

  // Compatibility of saved state
  if (gameState && gameState.gameState) {
    game = new Game(data, gameState.gameState)
    game.startDate = gameState.timestamp
  }
  else {
    game = new Game(data, gameState)
  }
  colors = Object.keys(game.colors).sort(sortColor)
  updateCheatCount()
}

function resetGame() {
  localStorage.setItem('puzzle' + puzzleHash, '')
  game = null
}


// --- State management --- 

function saveState(push) {
  try {
    var state = {
      selectedColor: selectedColor,
      data: game.data
    }
    if (push)
      history.pushState(state, '', '')
    else
      history.replaceState(state, '', '')
  }
  catch (e) {
    // This often happens when the state is too big
    console.error(e)
  }
}

function loadState(state) {
  loadGame(state.data)
  selectedColor = state.selectedColor
  displayAll()
}

function changePuzzle(data) {
  loadGame(data)
  selectedColor = colors[0]
  saveState(true)
  displayAll()
  togglePuzzlesBox(false)
}

function selectPuzzle(name) {
  $.get('./puzzles/' + name, changePuzzle, 'json')
}


// --- Startup / Cleanup ---

$(window).on('popstate', function(e) {
  loadState(e.originalEvent.state)
})

$(document).ready(function() {
  document.oncontextmenu = function() {
    return false
  }
  if (history.state)
    loadState(history.state)
  else
    selectPuzzle('wine.json')
  displayPuzzles()
  setInterval(updateStats, 100)
})

$(window).unload(function () {
  quitGame()
})


// --- Menu ---

$('#change-button,#cancel,#puzzle-title').on("click", function(e) {
  togglePuzzlesBox()
})

$('#reset-button').on("click", function(e) {
  var data = game.data
  resetGame()
  changePuzzle(data)
})

$('#download-button').on("click", function(e) {
  downloadPuzzle(game.data)
})

$('#pause-button').on("click", function(e) {
  game.togglePause()
  if (game.isPaused()) {
    pauseBox = spawnBox('Paused')
    hide($('#game'))
  }
  else {
    hide(pauseBox)
    show($('#game'))
  }
  saveGame()
})


// --- Puzzle selection ---

$('#localpuzzle').change(function(e) {
  loadPuzzle(this.files[0], changePuzzle)
})

/* Puzzle change click */
$('#puzzles').on('mousedown', '.puzzlink', function() {
  var name = $(this).attr('id')
  selectPuzzle(name)
})


// --- Color selection ---

$('#colors').on("click", ".color", function() {
  selectedColor = this.id.substring(6)
  saveState()
  generateHints()
  updateTiles()
  updateColors()
})


// --- Puzzle actions ---

/* Tile update */
function updateAt(x, y) {
  updateTileAt(x, y)
  updateHintsAt(x, y)
  updateCanvas(x, y)
}

function finalizeEvent(e) {
  updateColors()
  saveGame()
  e.preventDefault();
}

function changeTile(x, y, mode)
{
  if (x === undefined) {
    for (var x = 0; x < game.height; x++)
      changeTile(x, y, mode)
  }
  else if (y === undefined) {
    for (var y = 0; y < game.width; y++)
      changeTile(x, y, mode)
  }
  else {
    switch (mode) {
      case 'none':
        return

      case 'fill':
        if (game.isEmpty(y,x,selectedColor))
          game.set(y,x,selectedColor)
        break

      case 'erase':
        if (game.get(y,x) === selectedColor)
          game.erase(y,x)
        break

      case 'cross':
        if (game.isEmpty(y,x,selectedColor))
          game.cross(y,x,selectedColor)
        break

      case 'empty':
        if (game.get(y,x) === selectedColor)
          game.erase(y,x)
        // Continue to the next case !
      case 'uncross':
        /* Because the user is dump and WILL try to uncross blank columns */
        if (game.isFillable(y,x,selectedColor))
          game.uncross(y,x,selectedColor)
        break
    }
    updateAt(x, y)
    if (mode == 'fill' && game.isFinished())
      spawnBox('You win !', 4.0)
  }
}

function selectMode(button, x, y) {
  // If the tile is filled or crossed, the mode is to remove what's in
  // the tile, regardless of the button pressed
  if (x !== undefined && y !== undefined) {
    if (game.isCrossed(y,x,selectedColor))
      return 'uncross'
    else if (game.get(y,x) === selectedColor)
      return 'erase'
  }
  // Otherwise, we choose the mode depending on the button
  switch (button) {
  case 0: return 'fill'
  case 1: return 'empty'
  case 2: return 'cross'
  default: return 'none' // do nothing
  }
}

var mode = 'none'
/* Tile click handler */
$('#grid').on('mousedown', 'td', function(e) {
  var x = $(this).attr('data-x')
  var y = $(this).attr('data-y')
  mode = selectMode(e.button, x, y) // change global mode
  changeTile(x, y, mode)
  finalizeEvent(e)
  updateCursor($(this))
})

var x = 0, y = 0
$('#grid').on('mousemove', 'td', function(e) {
  var _x = $(this).attr('data-x')
  var _y = $(this).attr('data-y')
  if (_x != x || _y != y) {
    x = _x
    y = _y
    if (e.buttons != 0) {
      changeTile(x, y, mode)
      finalizeEvent(e)
    }
    updateCursor($(this))
  }
})

/* Remove error cheat */
$('#cheat-button').on("click", function(e) {
  var coord = game.removeError()
  if (coord)
    updateAt(coord.y, coord.x)
  updateCheatCount()
  finalizeEvent(e)
})

